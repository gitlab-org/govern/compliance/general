# Compliance feature directory for frontend engineers

## Description

This issue is intended for new frontend engineers in the compliance group. The list of GitLab features and the codebase is pretty enormous so I hope this serves as a comprehensive directory of all the features that compliance frontend engineering owns or regularly contributes to and where to find the relevant code and documentation. For more insight into **who** and **why** we're solving these problems for please see our [compliance direction handbook page](https://about.gitlab.com/direction/govern/compliance/compliance-management/).

### :mag: Audit events ([docs](https://docs.gitlab.com/ee/administration/audit_events.html))

> Available on: Instance, Groups and Projects

**How it's built:**
- Vue application ([link](https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/app/assets/javascripts/audit_events/components/audit_events_app.vue)) to render the table, filtering and sorting.
- Data gets rendered via HAML ([link](https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/app/views/admin/audit_logs/index.html.haml)) as a stringified data attribute.
   - Note: This was done as a first iteration.
   - Note: Once the audit events API ([docs](https://docs.gitlab.com/ee/api/audit_events.html)) has the required filtering https://gitlab.com/gitlab-org/gitlab/-/issues/363799 it could be used by the frontend.

**Notes for future engineers:**
- Audit event performance is slow and severely limiting! :turtle: 
- We currently only allow searching for entities, because the event details are saved in as a hash in the table.
- We have enforced the date-range to a maximum of 30 days.
- Future changes are impacted until this can be resolved. See https://gitlab.com/gitlab-org/gitlab/-/issues/375545.

#### :tv: Audit event streaming ([docs](https://docs.gitlab.com/ee/administration/audit_event_streaming.html))

> Available on: Top-level Groups

**How it's built:**
- Vue component ([link](https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/app/assets/javascripts/audit_events/components/audit_events_stream.vue)) that's part of the audit events app and uses GraphQL ([docs](https://docs.gitlab.com/ee/api/graphql/reference/#externalauditeventdestination)).

**Notes for future engineers:**
- Audit events streams are separate from the audit events table.
- As such there are no limitations that I'm aware of here. So this should be straightforward to expand.

### :bar_chart: Compliance report ([docs](https://docs.gitlab.com/ee/user/compliance/compliance_report/))

> Available on: Groups

**How it's built:**
- Vue app ([link](https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/app/assets/javascripts/compliance_dashboard/components/report.vue)) that fetches compliance violations using GraphQL ([docs](https://docs.gitlab.com/ee/api/graphql/reference/#complianceviolation)).

**Notes for future engineers:**
- We've also run into performance limitations on filtering compliance violations https://gitlab.com/gitlab-org/gitlab/-/issues/363357.
- This is the second iteration, with lots more to come. See https://gitlab.com/groups/gitlab-org/-/epics/2537.
- This could easily be made available on Projects. See https://gitlab.com/groups/gitlab-org/-/epics/5237#note_682417866.

### :robot: External status checks ([docs](https://docs.gitlab.com/ee/user/project/merge_requests/status_checks.html))

> Available on: Projects and Merge requests

**How it's built:**
- This feature consists of two parts:
   - Project settings for managing status checks.
   - Merge request widget that shows status checks results on individual MRs.

#### Project settings ([docs](https://docs.gitlab.com/ee/user/project/merge_requests/status_checks.html#view-the-status-checks-on-a-project))

**How it's built:**
- Vue app ([link](https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/app/assets/javascripts/status_checks/components/status_checks.vue)) that uses Rest API ([docs](https://docs.gitlab.com/ee/api/status_checks.html)).

#### Merge request widget ([docs](https://docs.gitlab.com/ee/user/project/merge_requests/status_checks.html#status-checks-widget))

**How it's built:**
- The widget ([link](https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/app/assets/javascripts/vue_merge_request_widget/extensions/status_checks/index.js)) is based on the MR widget extensions framework ([docs](https://docs.gitlab.com/ee/development/fe_guide/merge_request_widget_extensions.html)).
- @jiaan is currently the DRI for this widget ([docs](https://about.gitlab.com/handbook/engineering/development/dev/create/code-review/report-widgets-dri-list.html#merge-request-report-widgets---dri-list))

#### :star: Compliance frameworks ([docs](https://docs.gitlab.com/ee/administration/compliance.html))

> Available on: Groups and Projects

**How it's built:**
- Group setting is a Vue app ([link](https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/app/assets/javascripts/groups/settings/compliance_frameworks/components/table.vue)) using GraphQL ([docs](https://docs.gitlab.com/ee/api/graphql/reference/index.html#groupcomplianceframeworks)).
- Project setting is a simple HAML view ([link](https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/app/views/compliance_management/compliance_framework/_project_settings.html.haml)).

### :clipboard: Merge request approval settings ([docs](https://docs.gitlab.com/ee/user/admin_area/merge_requests_approvals.html))

> Available on: Instance, Groups, Projects and Merge requests

**How it's built:**
- Project setting Vue app ([link](https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/app/assets/javascripts/approvals/components/project_settings/app.vue)) that uses a Rest API ([docs](https://docs.gitlab.com/ee/api/merge_request_approvals.html)).
- Group setting Vue app ([link](https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/app/assets/javascripts/approvals/components/group_settings/app.vue)) that uses a Rest API ([docs](https://docs.gitlab.com/ee/api/merge_request_approvals.html)).
- Backend uses our in-house cascading setting framework ([docs](https://docs.gitlab.com/ee/development/cascading_settings.html#cascading-settings)).

### :lock: Deletion protection ([docs](https://docs.gitlab.com/ee/user/admin_area/settings/visibility_and_access_controls.html#deletion-protection))

> Available on: Instance and Groups

**How it's built:**
- Admin setting Vue app ([link](https://gitlab.com/gitlab-org/gitlab/-/blob/master/app/assets/javascripts/admin/application_settings/inactive_project_deletion/components/form.vue)) that acts as a selection of form fields to be submitted by the HAML form ([link](https://gitlab.com/gitlab-org/gitlab/-/blob/master/app/views/admin/application_settings/_repository_check.html.haml#L44))
   - Note: This was done because the form elements need to be dynamically enabled/disabled.
- Group setting is a HAML partial ([link](https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/app/views/groups/settings/_delayed_project_removal.html.haml)).
- Adds a banner to all project pages while deletion is pending ([link](https://gitlab.com/gitlab-org/gitlab/-/blob/master/app/views/shared/projects/_inactive_project_deletion_alert.html.haml))
- Backend uses our in-house cascading setting framework ([docs](https://docs.gitlab.com/ee/development/cascading_settings.html#cascading-settings)).
