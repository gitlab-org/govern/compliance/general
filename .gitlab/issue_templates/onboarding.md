## Onboarding / Realignment Information

**Start Date:2023-00-00**

**Team Member Name:`@`**

- Current Group: ``
- Current Engineering Manager: `@`
- New Group: `Govern::Compliance`
- New Engineering Manager: `@nrosandich`

---

<details>
<summary>Realignment</summary>

## Current Manager

- [ ] **Schedule** a meeting to discuss the transition with the affected team members
- [ ] **Post** a Goodbye / Thank you message to the group Slack channel
- [ ] **Initiate** [Workday transfer](https://docs.google.com/document/d/1JEobACNSMeHU4pU5DaaF7aqmv9wmSDoXx7veZ6_SM_I/edit) for team member
- [ ] **Verify** Expense Software is updated with the correct Manager
- [ ] **Verify** Workday is updated with the correct Manager
- [ ] **Walk through most recent Talent Assessment** with new manager: Ensure rating and context is shared and understood.
- [ ] **Remove** the new team member from all of the relevant Calendar Appointments
- [ ] **Remove** the team member from GitLab groups they no longer need to belong to 
- [ ] **Remove** the team member from any automated team standups (ex. Geekbot)
- [ ] **Create AR** to remove the team member from Google and Slack groups they no longer need to belong to
- [ ] **Schedule** a 1:1:1 transition call (where team member, previous manager, and new manager are all present)
- [ ] **I acknowledge that this transition call has taken place.**

## New Manager 
- [ ] **Schedule a meeting** to discuss the transition with the affected team members
- [ ] **Post a Welcome** message to group Slack channel
- [ ] **Update** the GitLab Org Chart  
- [ ] **Update or verify Workday has been updated** with the new team member
- [ ] **Add** the new team member to all of the relevant Slack Channels
- [ ] **Create AR** to add team member to the appropriate Google and Slack groups
- [ ] **Add** team member to appropriate GitLab groups and projects
- [ ] **Add** the new team member to all of the relevant Calendar Appointments
- [ ] **Schedule 1 to 1 Meetings** with the new team member
- [ ] **Review [Product Docs](https://docs.gitlab.com/ee/)** for new categories you might have aquired
- [ ] **Provide guidance** on how the team member should communicate and collaborate with their new team
- [ ] **Provide ongoing support** to the team members during the transition period, including regular check-ins and feedback sessions.
- [ ] Share your current OKRs
- [ ] **Share** group specific Handbook pages 
- [ ] **Share** group specific processes
- [ ] **Share** group specific communication methods (Weekly Announcements, etc)

## Team Member 
- [ ] **Meet** with your new manager
- [ ] **Share your Career Development** / goals with your new manager
- [ ] **Review** the new team's Handbook and Docs Pages ( [Direction Pages](https://about.gitlab.com/direction/#devsecops-stages), [Group & Category Information](https://about.gitlab.com/handbook/product/categories/),[Product Docs for the new categories supported by your group and stage](https://docs.gitlab.com/ee/))
- [ ] **Share** your work hours (including any country laws related to work schedule)
- [ ] **Document your accomplishments** completed since your last performance review
- [ ] **Document areas of improvements** you are currently working on
- [ ] **Update** your Slack Profile
</details>

## New Manager 
- [ ] **Post a Welcome** message to group Slack channel
- [ ] **Add** the new team member to all of the relevant Slack Channels
- [ ] **Create AR** to add team member to the appropriate Google and Slack groups
- [ ] **Add** team member to appropriate GitLab groups and projects
- [ ] **Add** the new team member to all of the relevant Calendar Appointments
- [ ] **Schedule 1 to 1 Meetings** with the new team member
- [ ] **Review [Product Docs](https://docs.gitlab.com/ee/)** for new categories you might have aquired
- [ ] **Provide guidance** on how the team member should communicate and collaborate with their new team
- [ ] **Provide ongoing support** to the team members during the transition period, including regular check-ins and feedback sessions.
- [ ] Share your current OKRs
- [ ] **Share** group specific Handbook pages 
- [ ] **Share** group specific processes
- [ ] **Share** group specific communication methods (Weekly Announcements, etc)

## Team Member 
- [ ] **Meet** with your new manager
- [ ] **Share your Career Development** / goals with your new manager
- [ ] **Review** the new team's Handbook and Docs Pages ( [Direction Pages](https://about.gitlab.com/direction/#devsecops-stages), [Group & Category Information](https://about.gitlab.com/handbook/product/categories/),[Product Docs for the new categories supported by your group and stage](https://docs.gitlab.com/ee/))
- [ ] **Share** your work hours (including any country laws related to work schedule)
- [ ] **Document your accomplishments** completed since your last performance review
- [ ] **Document areas of improvements** you are currently working on
- [ ] **Update** your Slack Profile

### Sec Section Onboarding

The [Sec Section](https://about.gitlab.com/direction/security/) encompasses the [Govern](https://about.gitlab.com/handbook/engineering/development/sec/govern/) and [Secure](https://about.gitlab.com/handbook/engineering/development/sec/secure/) stages.

The features provided by these teams are mostly leveraging tools that are executed during pipelines, using Docker images. That’s why it’s crucial to set up a development environment with the [GitLab CI Runner](https://gitlab.com/gitlab-org/gitlab-runner).

This document will guide you through the whole process. Don't hesitate to ask questions in Slack if anything is unclear:

* `#sec-section` for things relevant to the Govern and Secure Stages
* `#s_govern` for things related to the Govern Stage
* `#s_secure` for things related to the Secure Stage
* `#questions` for any general questions
* `#sec-section-social` to meet the rest of the section

Enjoy!

Need some help? Ping one of your [Sec team mates](https://about.gitlab.com/company/team/?department=sec-section), I'm sure they will be more than willing to answer your call!

#### General

1. [ ] Review the relevant handbook pages to understand how we fit in the organization structure, and what product categories we're responsible for developing and maintaining:
   1. [ ] [Direction for the Sec Section](https://about.gitlab.com/direction/security/)
   1. [ ] [Product Categories Hierarchy](https://about.gitlab.com/handbook/product/categories/#hierarchy) and find our Section, Stages, Groups and their respective Categories.
   1. [ ] [Secure and Govern Glossary of Terms](https://docs.gitlab.com/ee/user/application_security/terminology/) to get used to the terms you will frequently hear.

#### Govern

1. [ ] [Govern sub-department](https://about.gitlab.com/handbook/engineering/development/sec/govern/)
1. [ ] [Govern Stage](https://about.gitlab.com/handbook/product/categories/#govern-stage)
1. [ ] [Govern Stage Direction](https://about.gitlab.com/direction/govern/)

#### Compliance

* [ ] [Compliance Group](https://about.gitlab.com/handbook/engineering/development/sec/govern/compliance/)
* [ ] [Compliance Group Direction](https://about.gitlab.com/direction/govern/compliance/)
* [ ] [Compliance FE](https://gitlab.com/gitlab-org/govern/compliance/general/-/blob/main/FE.md)
* [ ] Read the relevant user documentation for the Compliance categories:
  * [ ] [Compliance Management](https://about.gitlab.com/direction/govern/compliance/compliance-management/)
    * [ ] [Compliance Frameworks](https://docs.gitlab.com/ee/user/group/compliance_frameworks.html#compliance-frameworks)
    * [ ] [Compliance Report](https://docs.gitlab.com/ee/user/compliance/compliance_report/index.html)
  * [ ] [Audit Events](https://about.gitlab.com/direction/govern/compliance/audit-events/)
    * [ ] [Audit Events](https://docs.gitlab.com/ee/administration/audit_events.html)
    * [ ] [Audit Event Guide](https://docs.gitlab.com/ee/development/audit_event_guide/)
    * [ ] [Audit Reports](https://docs.gitlab.com/ee/administration/audit_reports.html)
    * [ ] [Audit Event Streaming](https://docs.gitlab.com/ee/administration/audit_event_streaming.html)

#### Accounts

#### Govern

1. [ ] Manager: Add the new member to the [Govern Stage Calendar](https://calendar.google.com/calendar/u/0?cid=Z2l0bGFiLmNvbV9lZDYyMDd1ZWw3OGRlMGoxODQ5dmpqbmIza0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t) and explain what it is.
1. [ ] Manager: Add the new team member to the [team based Google Group](https://groups.google.com/my-groups)
1. [ ] Manager: Add to appropriate Slack channels
   * Engineering Slack `#sd_govern_engineering`
   * Frontend Slack `#sd_secure_govern_frontend`
   * Backend Slack `#sd_govern_backend`

#### Compliance

1. [ ] Manager: Add the new member to [https://staging.gitlab.com/compliance-tanuki](https://staging.gitlab.com/groups/compliance-tanuki/-/group_members) as Maintainer.
1. [ ] Manager: Add the new member to [https://gitlab.com/compliance-group-testing-and-demos](https://gitlab.com/groups/compliance-group-testing-and-demos/-/group_members) as Maintainer.
1. [ ] Manager: Add the new member to groups:
   * Compliance: https://gitlab.com/gitlab-org/govern/compliance
   * Compliance Engineering: https://gitlab.com/gitlab-org/govern/compliance/engineering

#### Docker

1. [ ] Install [a Docker Desktop alternative](https://about.gitlab.com/handbook/tools-and-tips/mac/#docker-desktop)
   1. [ ] If a more official recommendation has been made, open a Merge Request to update this template.

### Development Setup

You can choose to either or both of these. Note that GCK runs better on Linux than macOS.

1. [ ] [GDK (Gitlab Development Kit)](https://gitlab.com/gitlab-org/gitlab-development-kit/-/tree/main) provides a standard development environment commonly used by Gitlab engineers.

   <details>
   <summary>GDK</summary>
   1. [ ] GDK
      1. [ ] Install all needed [dependencies for GDK](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/index.md). Watch [video on GDK installation](https://www.youtube.com/watch?v=gxn-0KSfNaU).
      1. [ ] Change your local GitLab hostname to `gitlab.localdev`
         1. [ ] Add `127.0.0.1 gitlab.localdev` line to `/etc/hosts` file. This creates an alias for `127.0.0.1` to `gitlab.localdev`.
         1. [ ] Add `hostname: gitlab.localdev` to `gdk.yml`
         1. [ ] Run `gdk reconfigure` and then `gdk restart`. After this, you should be able to access your local GitLab instance at `http://gitlab.localdev:3000`.
      1. [ ] Visit http://gitlab.localdev:3000/users/sign_in and login as an admin user with username `root` and password `5iveL!fe`. You will need to change the password after your first login.
      1. [ ] Upload your Ultimate license key for GitLab Enterprise Edition by visiting `http://gitlab.localdev:3000/admin/application_settings/general` and going to `Add License`. If you do not have GitLab Ultimate license, follow instructions [here](https://about.gitlab.com/handbook/developer-onboarding/#working-on-gitlab-ee-developer-licenses) to obtain a license file.
      1. [ ] If you’re stuck, ask for help in `#development`, `#gdk` Slack channels. Use Slack search for your questions. first with filter `in:#gdk`. There is a possibility that someone has already had a similar issue.
      1. [ ] Check [How to use GitLab Development Kit doc](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/main/doc/howto/index.md)
      1. [ ] You’ll need to know [commands](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/main/HELP) to operate your local environment successfully. For example, to start database locally (to run tests), you need to start db with `gdk start postgresql redis`. For our purposes, run everything excluding the runners with `gdk start && gdk stop runner` because we'll run those separately in the steps below.
      1. [ ] GDK contains a collection of resources that help running an instance of GitLab locally as well as GitLab codebase itself. The GitLab code can be found in `/gitlab` folder of GDK. Check this folder.
      1. [ ] If you have not already, create a reminder for 3 months out from your start date to add yourself as a reviewer for the gitlab project by adding a `projects` key to your entry in gitlab.com's `data/team.yml`. This will allow you to be automatically suggested as a reviewer for MRs.
   1. [ ] Runner
      1. [ ] Install GitLab Runner locally with [this tutorial](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/main/doc/howto/runner.md). Be sure to follow the Docker related setup instructions. (Optionally: you can setup your Runners to run inside containers by following these [instructions](https://docs.gitlab.com/runner/install/docker.html#docker-image-installation-and-configuration))
      1. [ ] Register your runner with the command: `gitlab-runner register -c <gdk-path>/gitlab-runner-config.toml`, choose `Docker` as an executor and http://gitlab.localdev:3000/ as the coordinator URL. This command will generate the `gitlab-runner-config.toml` file.
      1. [ ] Add your runner `token` and set `extra_hosts` inside `gdk.yml`. It should have the following contents with your runner's token in place of `<token>`:

         ```yaml
         hostname: gitlab.localdev
         runner:
           token: <runner-token>
           extra_hosts:
             - gitlab.localdev:192.168.99.1
         ```
      1. [ ] Run `gdk reconfigure`.
      1. [ ] Run the GitLab Runner in the foreground with the command: `gitlab-runner --log-level debug run --config <gdk-path>/gitlab-runner-config.toml`. If your Runner is setup to run inside docker container then run `docker run gitlab/gitlab-runner --log-level debug run --config <gdk-path>/gitlab-runner-config.toml`
      1. [ ] Create a [Loopback Interface](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/runner.md#set-up-docker-and-gdk) to make sure docker and the gdk find each other. Also, ensure to remove any previous mapping for `gitlab.localdev` before mapping it to `172.16.123.1` inside `/etc/hosts` file. Do not change `gitlab.rails.hostname` in `gdk.yml`.
      1. [ ] Putting it all together, your final `gdk.yml` file should look something like this:

         ```yaml
         hostname: gitlab.localdev
         runner:
           enabled: true
           executor: docker
           install_mode: docker
           token: <runner-token>
           extra_hosts:
             - gitlab.localdev:172.16.123.1
         ```
      1. [ ] If you have questions about the runner or you're stuck and need help, ask in `#g_runner` Slack channel.
   </details>
1. [ ] [GCK (Gitlab Compose Kit)](https://gitlab.com/gitlab-org/gitlab-compose-kit) is an alternative development environment for those familiar with Docker. It works better for Linux than macOS.
   1. [ ] Full installation instructions for the GCK can be found [here](https://gitlab.com/gitlab-org/gitlab-compose-kit#use-it).
   1. [ ] The GCK has an automatically configured Gitlab Runner built in, but this might not cooperate with your firewall settings. Consider joining the [#gck](https://app.slack.com/client/T02592416/CDPA9TK1B) slack channel if you require assistance.

#### Prepare to contribute

1. [ ] Configure GPG to [sign your commits](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/).
1. [ ] Create an MR with improvements to this [document](https://gitlab.com/gitlab-org/secure/onboarding/blob/master/.gitlab/issue_templates/Technical_Onboarding.md).
1. [ ] Note the existence and location of the [security issue workflow](https://about.gitlab.com/handbook/engineering/workflow/#security-issues). You are unlikely to work on a security fix as your first task, but it's important to know that security MRs have a different workflow.
1. [ ] Record a quick demo video of a GitLab feature or of a development tip/trick that you use. Post the video to [GitLab unfiltered](https://about.gitlab.com/handbook/marketing/marketing-operations/youtube/#post-everything) or upload it to Google Drive, and share a link to the video on #sec-section.
1. [ ] Read the [Sec Section development documentation](https://docs.gitlab.com/ee/development/sec/), as it contains a great wealth of information that helps understand the mechanics of how our code works.

#### Understand GitLab Infrastructure Better

1. [ ] [Review GitLab Monitoring Tools and test them out](https://www.youtube.com/playlist?list=PL05JrBw4t0KpQMEbnXjeQUA22SZtz7J0e)
1. [ ] [Review Visualization Tools and test them](https://www.youtube.com/playlist?list=PL05JrBw4t0KrDIsPQ68htUUbvCgt9JeQj) If you can not access the above playlists because you get 'This playlist is private.', go to the top right of the screen in Youtube and click 'Switch Account' > 'GitLab Unfiltered'

### Now you are ready for new tasks

You're awesome!
