## :palm_tree: OOO Issue :palm_tree: 

### When

Example: August 17th, 2023 to August 29th, 2023

### Checklist

- [ ] Re-assigned or communicated open work which requires coverage
- [ ] Time Off by Deel entered
- [ ] Not on-call during PTO
- [ ] Updated Google Calendar (decline meetings or set yourself out of office and auto-decline events)
- [ ] Updated GitLab Profile to :palm_tree: (should already been done from Time Off by Deel)

### Ongoing work :construction: 

#### Issues:

| Issue | Workflow | Notes |
|-|-|-|
| | | |

#### MRs:

| MR | Workflow | Notes |
|-|-|-|
| | | |


### Additional Notes
