#### This issue is for weekly updates during Milestone %"15.11" 

At the start of each week EM will post a comment with updates for the Compliance team.


----

<details>
<summary>Weekly updates with the following titles (as needed) will be added in comments</summary>

````
## Update Week Start 1st Month

#### :book: Weekly Docs
------

:newspaper: [Engineering Week In Review](https://docs.google.com/document/d/1JBdCl3MAOSdlgq3kzzRmtzTsFWsTIQ9iQg0RHhMht6E/edit)

:newspaper: [Compliance Meeting Agenda](https://docs.google.com/document/d/1jzTYJ_COHJMzBYFiBiiGUWnAzWiWB-H06pPf6kiBgdQ/edit#)

#### :sun_with_face: Family & Friends Day
------

:newspaper: Upcoming Family and Friends Days (See [the handbook page](https://about.gitlab.com/company/family-and-friends-day/))

#### :writing_hand:  Team Updates
------

:newspaper: 

#### :globe_with_meridians:  Company Updates
------

:newspaper: 

#### :wrench:  Eng Updates
------

:newspaper: 

:newspaper: https://gitlab.com/gitlab-com/software_supply_chain_security-sub-department/-/issues/221+ :bar_chart:

#### :rocket:  Product Updates
------

:newspaper: 

:newspaper: [OST](https://gitlab.com/groups/gitlab-org/software-supply-chain-security/compliance/-/epics/19) :octopus:


:newspaper: Report of the last week's completed issues https://gitlab.com/gitlab-org/software-supply-chain-security/compliance/general/-/issues/145+ :chart_with_upwards_trend:


#### :speech_balloon: Opened discussion
------

:newspaper: 

FYI `@gitlab-org/software-supply-chain-security/compliance`
````

</details>

/assign @nrosandich
/label ~"weekly update"  ~"group::compliance" ~"type::ignore"
/milestone %
/epic https://gitlab.com/groups/gitlab-org/software-supply-chain-security/compliance/-/epics/3
